﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Timers;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Threading;
using System.Xml.Serialization;

namespace MyGame
{
    public partial class MainWindow : Window
    {
        DispatcherTimer gameTimer = new DispatcherTimer();
        DispatcherTimer timerNLO;
        DispatcherTimer timerHole = new DispatcherTimer();
        
        bool isJump = false;
        bool isSit = false;
        Rect Cat;
        int score = 0;
        int a = 0;
        DispatcherTimer timerCat;
        DoubleAnimation jumpUp = new DoubleAnimation();
        DoubleAnimation jumpDown = new DoubleAnimation();
        DoubleAnimation animationNLO = new DoubleAnimation();
        DoubleAnimation animationHole = new DoubleAnimation();
        BitmapImage ImageCat1 = new BitmapImage(new Uri(@"pack://application:,,,/Images/cat1.png"));
        BitmapImage ImageCat2 = new BitmapImage(new Uri(@"pack://application:,,,/Images/cat2.png"));
        BitmapImage ImageCat3 = new BitmapImage(new Uri(@"pack://application:,,,/Images/cat3.png"));
        BitmapImage ImageHole1 = new BitmapImage(new Uri(@"pack://application:,,,/Images/hole.png"));
        BitmapImage ImageHole2 = new BitmapImage(new Uri(@"pack://application:,,,/Images/hole2.png"));
        const double TIME1 = 20;
        const double TIME2 = 150;
        const double TIME3 = 3000;

        public MainWindow()
        {
            InitializeComponent();
            MyCanvas1.Visibility = Visibility.Visible;
        }
        private void StartGame()
        {
            #region Timers
            timerCat = new DispatcherTimer();
            timerCat.Interval = TimeSpan.FromMilliseconds(TIME2);
            timerCat.Tick += Animation;
            timerCat.Start();

            gameTimer.Interval = TimeSpan.FromMilliseconds(TIME1);
            gameTimer.Tick += GameEngine;
            gameTimer.Start();

            timerHole.Interval = TimeSpan.FromMilliseconds(TIME3);
            timerHole.Tick += Anim2;
            timerHole.Start();

            timerNLO = new DispatcherTimer();
            timerNLO.Interval = TimeSpan.FromMilliseconds(3000);
            timerNLO.Tick += Anim3;
            timerNLO.Start();
            #endregion 

            #region Animations
            jumpUp.Completed += SecondAnimation;
            jumpUp.From = Canvas.GetTop(cat1);
            jumpUp.To = 335; //330
            jumpUp.Duration = TimeSpan.FromSeconds(0.3); //0.3

            jumpDown.Completed += CompleteAnim;
            jumpDown.From = 335;
            jumpDown.To = Canvas.GetTop(cat1);
            jumpDown.Duration = TimeSpan.FromSeconds(0.3);

            animationNLO.Completed += SecondAnimation2;
            animationNLO.From = Canvas.GetLeft(NLO);
            animationNLO.To = -93;
            animationNLO.Duration = TimeSpan.FromSeconds(1.5);

            animationHole.From = Canvas.GetLeft(HOLE);
            animationHole.To = -93;
            animationHole.Duration = TimeSpan.FromSeconds(3);
            #endregion

            score = 0;
            gameTimer.Start();
        }
        private void btStart_Click(object sender, RoutedEventArgs e)
        {
            MyCanvas1.Visibility = Visibility.Hidden;
            MyCanvas3.Visibility = Visibility.Hidden;
            MyCanvas4.Visibility = Visibility.Hidden;
            MyCanvas2.Visibility = Visibility.Visible;
            StartGame();
        }
        private void GameEngine(object sender, EventArgs e)
        {
            Cat = new Rect(Canvas.GetLeft(cat1), Canvas.GetTop(cat1), cat1.Width - 40, cat1.Height - 6);
            Canvas.SetTop(cat1, 474);
            foreach (var x in MyCanvas2.Children.OfType<Image>())
            {
                if ((string)x.Tag == "hole")
                {
                    Rect pillars = new Rect(Canvas.GetLeft(x), Canvas.GetTop(x), x.Width - 12, x.Height / 2);
                    if (Cat.IntersectsWith(pillars))
                    {
                        HOLE.BeginAnimation(Canvas.LeftProperty, null);
                        NLO.BeginAnimation(Canvas.LeftProperty, null);
                        gameTimer.Stop();
                        timerHole.Stop();
                        timerCat.Stop();
                        cat1.Source = ImageCat1;
                        GameOver();
                    }
                }
                if ((string)x.Tag == "nlo")
                {
                    Rect pillars = new Rect(Canvas.GetLeft(x), Canvas.GetTop(x), x.Width - 12, x.Height - 10);
                    if (isSit)
                    {
                        Cat.Height = Cat.Height / 2;
                        Cat.Y = Canvas.GetTop(cat1) + 20;
                    }
                    else
                    {
                        Cat.Height = cat1.Height - 6;
                    }
                    if (Cat.IntersectsWith(pillars))
                    {
                        HOLE.BeginAnimation(Canvas.LeftProperty, null);
                        NLO.BeginAnimation(Canvas.LeftProperty, null);
                        gameTimer.Stop();
                        timerHole.Stop();
                        timerCat.Stop();
                        timerNLO.Stop();
                        cat1.Source = ImageCat1;
                        GameOver();
                    }
                }
            }
        }
        List<Player> player = new List<Player>();
        private void GameOver()
        {
            StreamReader sr = new StreamReader("records.txt");
            string s = sr.ReadLine();
            sr.Close();
            if (s != null)
            {
                XmlSerializer xml2 = new XmlSerializer(typeof(List<Player>),
                    new Type[] { typeof(Player) });
                using (Stream fstream = File.OpenRead("records.txt"))
                {
                    player = (List<Player>)xml2.Deserialize(fstream);
                }
            }
            player.Add(new Player(score, TextBoxName.Text));
            XmlSerializer xml = new XmlSerializer(typeof(List<Player>),
            new Type[] { typeof(Player) });
            using (Stream file =  new FileStream("records.txt", FileMode.Create, FileAccess.Write, FileShare.None))
            {
                xml.Serialize(file, player);
            }
            MyCanvas3.Visibility = Visibility.Visible;
        }
        private void Animation(object sender, EventArgs e)
        {
            score += 1;
            tbScore.Text = score.ToString();
            if (!isJump && !isSit)
            {
                if (a == 0)
                {
                    cat1.Source = ImageCat2;
                    a = 1;
                }
                else
                {
                    cat1.Source = ImageCat1;
                    a = 0;
                }
            }
        }
        static Random rnd;
        private void Anim2(object sender, EventArgs e)
        {
            rnd = new Random((int)(DateTime.Now.Ticks));
            int r = rnd.Next(0, 20);
            if (r%2 == 1)
            {
                HOLE.Source = ImageHole1;
            }
            else
            {
                HOLE.Source = ImageHole2;
            }
            HOLE.BeginAnimation(Canvas.LeftProperty, animationHole);
        }
        private void Anim3(object sender, EventArgs e)
        {
            NLO.BeginAnimation(Canvas.LeftProperty, animationNLO);
        }
        private void btExit_Click(object sender, RoutedEventArgs e)
        {
            WindowExit exit = new WindowExit();
            exit.Show();
        }
        private void Window_Closed(object sender, EventArgs e)
        {
            Application.Current.Shutdown();
        }
        private void SecondAnimation(object sender, EventArgs e)
        {
            cat1.BeginAnimation(Canvas.TopProperty, jumpDown);
        }
        private void SecondAnimation2(object sender, EventArgs e)
        {
            timerNLO.Stop();
            NLO.BeginAnimation(Canvas.LeftProperty, null);
            Canvas.SetLeft(NLO, 1060);
            animationNLO.From = Canvas.GetLeft(NLO);
            animationNLO.To = -93;
            animationNLO.Duration = TimeSpan.FromSeconds(3);
            timerNLO = new DispatcherTimer();
            timerNLO.Interval = TimeSpan.FromMilliseconds(3000);
            timerNLO.Tick += Anim3;
            timerNLO.Start();
        }
        private void CompleteAnim(object sender, EventArgs e)
        {
            cat1.Source = ImageCat1;
            isJump = false;
        }
        private void Window_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Down && !isJump)
            {
                cat1.Source = ImageCat1;
                isSit = false;
            }
        }
        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Down && !isJump)
            {
                cat1.Source = ImageCat3;
                isSit = true;
            }
            if (e.Key == Key.Up && !isJump )
            {
                isJump = true;
                cat1.Source = ImageCat2;
                cat1.BeginAnimation(Canvas.TopProperty, jumpUp);
            }
        }
        private void btRestart_Click(object sender, RoutedEventArgs e)
        {
            MyCanvas1.Visibility = Visibility.Hidden;
            MyCanvas3.Visibility = Visibility.Hidden;
            MyCanvas2.Visibility = Visibility.Visible;
            StartGame();
        }
        private void btChange_Click(object sender, RoutedEventArgs e)
        {
            if (!TextBoxName.IsEnabled)
            {
                TextBoxName.IsEnabled = true;
                btChange.Content = "Ok";
            }
            else
            {
                TextBoxName.IsEnabled = false;
                btChange.Content = "Change";
            }
            
        }
        private void btRecords_Click(object sender, RoutedEventArgs e)
        {
            MyCanvas4.Visibility = Visibility.Visible;
            MyCanvas1.Visibility = Visibility.Hidden;
            MyCanvas3.Visibility = Visibility.Hidden;
            MyCanvas2.Visibility = Visibility.Hidden;
            ListBoxRecords.Items.Clear();
            List<Player> players = new List<Player>();
            StreamReader sr = new StreamReader("records.txt");
            string s = sr.ReadLine();
            if (s != null)
            {
                XmlSerializer xml = new XmlSerializer(typeof(List<Player>),
                    new Type[] { typeof(Player) });
                using (Stream fstream = File.OpenRead("records.txt"))
                {
                    players = (List<Player>)xml.Deserialize(fstream);
                }
            }
            
            Player p = new Player();
            players = p.SortPlayer(players);
            
            int i = 0;
            foreach (var item in players)
            {
                i++;
                ListBoxRecords.Items.Add($"{i}. {item.Name} {item.Score}");
            }
        }
        private void Button_Click(object sender, RoutedEventArgs e)
        {

        }
        private void Back_Click_1(object sender, RoutedEventArgs e)
        {
            MyCanvas4.Visibility = Visibility.Hidden;
            MyCanvas1.Visibility = Visibility.Visible;
            MyCanvas3.Visibility = Visibility.Hidden;
            MyCanvas2.Visibility = Visibility.Hidden;
        }
        private void Menu_Click_1(object sender, RoutedEventArgs e)
        {
            MyCanvas4.Visibility = Visibility.Hidden;
            MyCanvas1.Visibility = Visibility.Visible;
            MyCanvas3.Visibility = Visibility.Hidden;
            MyCanvas2.Visibility = Visibility.Hidden;
        }
    }
}
