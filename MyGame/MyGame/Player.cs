﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyGame
{
    public class Player
    {
        public int Score { get; set; }
        public string Name { get; set; }
        public Player(int score, string name)
        {
            Score = score;
            Name = name; //dfsklfkdsgksgwoutwofgw
        }
        public Player() {}
        public List<Player> SortPlayer(List<Player> players)
        {
            var selectedPlayers = players.OrderByDescending(x => x.Score).GroupBy(x => x.Name).Select(x => x.First()).ToList();
            if (selectedPlayers.Count >= 3)
            {
                selectedPlayers.RemoveRange(3, selectedPlayers.Count - 3);
            }
            return selectedPlayers;
        }
    }
}
